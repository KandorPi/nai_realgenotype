package pjwstk.nai_realgenotype;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    Button b1, b2;
    public EditText a, b, fi, x;
    public TextView result;
    public int genLength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        a = (EditText)findViewById(R.id.editText);
        b = (EditText)findViewById(R.id.editText2);
        fi = (EditText)findViewById(R.id.editText3);
        x = (EditText)findViewById(R.id.editText4);
        result = (TextView)findViewById(R.id.result);
        b1 = (Button)findViewById(R.id.button);
        b2 = (Button)findViewById(R.id.button2);
        b1.setOnClickListener(clickHandler);
        b2.setOnClickListener(clickHandler2);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener clickHandler = new View.OnClickListener(){
        public void onClick(View v){
            if(a.getText().toString().matches("") || b.getText().toString().matches("") || x.getText().toString().matches("") || fi.getText().toString().matches("")){
                return;
            }
            genLength = getGenotypeLength(Integer.parseInt(a.getText().toString()), Integer.parseInt(b.getText().toString()), Integer.parseInt(fi.getText().toString()));
            String gen = getGenotype(Integer.parseInt(a.getText().toString()), Integer.parseInt(b.getText().toString()), Double.parseDouble(x.getText().toString()), genLength);
            result.setText(gen);
        }
    };

    View.OnClickListener clickHandler2 = new View.OnClickListener(){
        public void onClick(View v){
            a.setText("");
            b.setText("");
            fi.setText("");
            x.setText(" ");
        }
    };

    public static int getGenotypeLength(int a, int b, int fi) {
        double result = 0;
        double r = (b-a)*(Math.pow(10, fi));
        result = (Math.log(r) / Math.log(2)); // <- implemented log2(r) !
        result = Math.ceil(result);
        int finalResult = (int) result;

        return finalResult;
    }

    public static String getGenotype(int a, int b, double x, int genLength) {

        double r = (x-a)*(Math.pow(2, genLength) -1);
        r /= (b-a);
        int result = roundIt(r);
        String s = Integer.toBinaryString(result);

        if(genLength != s.length()) {
            int difference = genLength - s.length();
            String prefix = "";
            for(int i=0; i <difference; i++) {
                prefix += "0";
            }
            prefix +=s;
            s = prefix;
        }

        return s;
    }

    public static boolean checkPrecision(int a, int b, String genotype, int assumedFi) {

        double n = (b-a) * (Math.pow(10, assumedFi));

        n = (Math.log(n) / Math.log(2)); // log2(n) <--
        n = Math.ceil(n);

        System.out.println("supposed length: " + n);

        boolean isGood = (n == genotype.length()) ? true : false;

        return isGood;
    }

    /**
     * rounds 'd' number to the closest integer value
     * @param d
     * @return - rounded number
     */
    public static int roundIt(double d) {

        int a = 0;
        double temp = Math.abs(d);

        a = (int) Math.round(temp);

        if(d < 0) {
            a *= -1;
        }

        return a;
    }
}
